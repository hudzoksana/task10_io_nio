package com.hudz.controller;

import com.hudz.model.DisplayContentOfDirectoryTask;
import com.hudz.model.FindCommentsTask.FindComments;
import com.hudz.model.MyInputStream.ReadUnreadMyInputStream;
import com.hudz.model.ResourcePath;
import com.hudz.model.SerializableTask.SerializeTask;
import com.hudz.model.SerializableTask.StudentsList;
import com.hudz.model.TaskComparingRWPerfomance;

import java.io.File;
import java.util.List;

public class ControllerImpl extends Controller {
    private SerializeTask serializeTask = new SerializeTask();
    private TaskComparingRWPerfomance taskComparingRWPerfomance = new TaskComparingRWPerfomance();
    private ReadUnreadMyInputStream readUnreadMyInputStream = new ReadUnreadMyInputStream();
    private FindComments findComments = new FindComments();
    private DisplayContentOfDirectoryTask displayContentOfDirectoryTask = new DisplayContentOfDirectoryTask();

    @Override
    public void getResultSerializeDeserializeObj() {
        logger.trace("Object to serialize: \n" + StudentsList.getStudent().toString());
        serializeTask.serialize(StudentsList.getStudent());
        logger.trace("Serialized");
        logger.trace("Deserialized:\n" + serializeTask.deserialize().toString());
    }

    @Override
    public String getResultOfComparing() {
        return taskComparingRWPerfomance.getResultOfComparing();
    }

    @Override
    public String getResultFromReadUnreadMyInputStream() {
        return readUnreadMyInputStream.getResultFromReadUnreadMyInputStream();
    }

    @Override
    public List<String> getResultOfSearchingComments() {
        return findComments.getComments(ResourcePath.INSTANCE.getString("pathComments"));
    }

    @Override
    public void showContentInDirectory() {
        displayContentOfDirectoryTask.showContentInDirectory(ResourcePath.INSTANCE.getString("pathDirectory"));
    }
}
