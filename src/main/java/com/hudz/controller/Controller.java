package com.hudz.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public abstract class Controller {

    public final Logger logger = LogManager.getLogger(Controller.class);
    public abstract void getResultSerializeDeserializeObj();

    public abstract String getResultOfComparing();

    public abstract String getResultFromReadUnreadMyInputStream();

    public abstract List<String> getResultOfSearchingComments();

    public abstract void showContentInDirectory();
}
