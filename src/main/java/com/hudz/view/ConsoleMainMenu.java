package com.hudz.view;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.ResourceBundle;

public class ConsoleMainMenu extends AbstractMenu {

  private void setMenu() {

    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("6"));
    menu.put("7", bundle.getString("7"));

    menu.put("Q", bundle.getString("Q"));
  }

  public ConsoleMainMenu() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    methodsMenu = new LinkedHashMap<>();

    methodsMenu.put("1", this::getResultSerializeDeserializeObj);
    methodsMenu.put("2", this::getResultOfComparing);
    methodsMenu.put("3", this::getResultFromReadUnreadMyInputStream);
    methodsMenu.put("4", this::getResultOfSearchingComments);
    methodsMenu.put("5", this::showContentInDirectory);
//    methodsMenu.put("6", this::);
//    methodsMenu.put("7", this::);
  }

  private void showContentInDirectory() {
    controller.showContentInDirectory();
  }

  private void getResultOfSearchingComments() {
    logger.trace(controller.getResultOfSearchingComments());
  }

  private void getResultFromReadUnreadMyInputStream() {
      logger.trace(controller.getResultFromReadUnreadMyInputStream());
  }

  private void getResultOfComparing() {
    logger.trace(controller.getResultOfComparing());
  }

  private void getResultSerializeDeserializeObj() {
    controller.getResultSerializeDeserializeObj();
  }


  private void internationalizeMenuUkrainian() {
    locale = new Locale("uk");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }

  private void internationalizeMenuEnglish() {
    locale = new Locale("en");
    bundle = ResourceBundle.getBundle("MyMenu", locale);
    setMenu();
    show();
  }
}
