package com.hudz.model.SerializableTask;

import java.io.Serializable;
import java.util.Objects;

public class Group implements Serializable {
    public String fuculty;
    public int number;
    public transient int counter;

    public Group(String fuculty, int number, int counter) {
        this.fuculty = fuculty;
        this.number = number;
        this.counter = counter;
    }
    public String getFuculty() {
        return fuculty;
    }

    public void setFuculty(String fuculty) {
        this.fuculty = fuculty;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        Group group = (Group) o;
        return getCounter() == group.getCounter() &&
                Objects.equals(getFuculty(), group.getFuculty()) &&
                Objects.equals(getNumber(), group.getNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFuculty(), getNumber(), getCounter());
    }

    @Override
    public String toString() {
        return "Group{" +
                "fuculty='" + fuculty + '\'' +
                ", number='" + number + '\'' +
                ", counter=" + counter +
                '}';
    }
}
