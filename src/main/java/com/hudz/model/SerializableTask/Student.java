package com.hudz.model.SerializableTask;

import java.io.Serializable;
import java.util.Objects;

public class Student implements Serializable {
    private int id;
    private String surname;
    private Group group;
    public transient int number;

    public Student(int id, String surname, Group group, int number) {
        this.id = id;
        this.surname = surname;
        this.group = group;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSurname(), getGroup(), getNumber());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getId() == student.getId() &&
                getNumber() == student.getNumber() &&
                Objects.equals(getSurname(), student.getSurname()) &&
                Objects.equals(getGroup(), student.getGroup());
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", group=" + group +
                ", number=" + number +
                '}';
    }
}
