package com.hudz.model.SerializableTask;
import com.hudz.model.ResourcePath;

import java.io.*;

public class SerializeTask {
    private String path = ResourcePath.INSTANCE.getString("pathForSerializing");

    public void serialize(Student student) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path))) {
            out.writeObject(student);
        } catch (IOException e) {
            e.printStackTrace();

        }
    }
        public Student deserialize () {
            try (ObjectInputStream in =
                         new ObjectInputStream(new FileInputStream(path))) {
                return (Student) in.readObject();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

