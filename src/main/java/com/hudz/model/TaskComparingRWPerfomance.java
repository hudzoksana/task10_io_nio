package com.hudz.model;

import java.io.*;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

public class TaskComparingRWPerfomance {
    private String path = ResourcePath.INSTANCE.getString("pathBook");

    public long readFileUsual() {
        long start = System.nanoTime();
        try(InputStream inputStream = new FileInputStream(path)) {
                int text = inputStream.read();
                while (text != -1) {
                    text = inputStream.read();
                }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public long readWithBuffer(int size) {
        long start = System.nanoTime();
        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(path), size)) {
            int text = inputStream.read();
            while (text != -1) {
                text = inputStream.read();
            }
            return System.nanoTime() - start;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String getResultOfComparing() {
        return "Time without buffer: " + readFileUsual() + "\n" +
                "Time with buffer 200MB: " + readWithBuffer(1024 * 1024 * 200) + "\n" +
                "Time with buffer 100MB: " + readWithBuffer(1024 * 1024 * 100) + "\n" +
                "Time with buffer 50MB: " + readWithBuffer(1024 * 1024 * 50) + "\n" +
                "Time with buffer 25MB: " + readWithBuffer(1024 * 1024 * 25) + "\n" +
                "Time with buffer 10MB: " + readWithBuffer(1024 * 1024 * 10) + "\n" +
                "Time with buffer 5MB: " + readWithBuffer(1024 * 1024 * 5) + "\n" +
                "Time with buffer 1MB: " + readWithBuffer(1024 * 1024) + "\n" +
                "Time with buffer 100KB: " + readWithBuffer(1024 * 100) + "\n";
    }


}
