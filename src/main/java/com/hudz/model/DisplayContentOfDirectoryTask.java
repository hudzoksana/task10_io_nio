package com.hudz.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DisplayContentOfDirectoryTask {

    private final static Logger logger = LogManager.getLogger(DisplayContentOfDirectoryTask.class);

    public void showContentInDirectory(String path) {
        try {
            Files.walk(Paths.get(path), FileVisitOption.FOLLOW_LINKS)
                    .map(Path::toFile)
                    .forEach(f -> {
                        System.out.println(f.getName() + (f.isDirectory() ? " directory" : " file"));
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
