package com.hudz.model;

import java.util.Locale;
import java.util.ResourceBundle;

public enum ResourcePath {
    INSTANCE;
    private ResourceBundle resourceBundle;
    private final String resourceName = "paths";

    ResourcePath() {
        resourceBundle = ResourceBundle.getBundle(resourceName,
                Locale.getDefault());
    }

    public String getString(final String key) {
        return resourceBundle.getString(key);
    }
}
