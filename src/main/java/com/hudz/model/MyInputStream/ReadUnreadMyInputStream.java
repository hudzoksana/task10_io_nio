package com.hudz.model.MyInputStream;

import com.hudz.model.ResourcePath;

import java.io.FileInputStream;
import java.io.IOException;

public class ReadUnreadMyInputStream {

    private String path = ResourcePath.INSTANCE.getString("pathBook");
    private StringBuilder result = new StringBuilder();
    private int sizeOfBuffer = 1024 * 1024 * 50;

    public String getResultFromReadUnreadMyInputStream() {
        try (MyInputStream inp =
                     new MyInputStream(new FileInputStream(path),
                             sizeOfBuffer)) {
            int b = inp.read();
            result.append("Read: ").append((char) b).append("\n");
            inp.unread(b);
            result.append("Unread: ").append((char)inp.read()).append("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}

