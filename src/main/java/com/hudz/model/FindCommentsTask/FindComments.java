package com.hudz.model.FindCommentsTask;

import org.antlr.runtime.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FindComments {

    public List<String> getComments (String path) {
        List<String> comments = new ArrayList<>();
        StringBuilder comment = new StringBuilder();

        try {
            StreamTokenizer tokenizer = new StreamTokenizer(
                    new FileInputStream(path));
            tokenizer.ordinaryChar('/');
            tokenizer.eolIsSignificant(true);
            while(tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
                if (tokenizer.ttype == '/' || tokenizer.ttype == '*') {
                    while (tokenizer.nextToken() != StreamTokenizer.TT_EOL) {
                        if (tokenizer.ttype == StreamTokenizer.TT_WORD || tokenizer.ttype == StreamTokenizer.TT_NUMBER) {
                            comment.append(tokenizer.sval).append(" ");
                        }
                    }
                    if (!comment.toString().isEmpty()) {
                        comments.add(comment.toString().trim());
                        comment = new StringBuilder();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return comments;
    }
}
