package com.hudz;

import com.hudz.view.AbstractMenu;
import com.hudz.view.ConsoleMainMenu;

public class Application {
    public static void main(String[] args) {
        AbstractMenu menu = new ConsoleMainMenu();
        menu.show();
    }
}
